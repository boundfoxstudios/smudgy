export enum Events {
  Register = 'register',
  CreateSession = 'create-session',
  JoinSession = 'join-session',
  PlayerLeaveSession = 'player-leave-session',
}
