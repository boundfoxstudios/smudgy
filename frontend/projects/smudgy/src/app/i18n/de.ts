import { Translations } from './base';

export const TRANSLATIONS_DE: Translations = {
  LOBBY: {
    SETTINGS: {
      TITLE: 'Einstellungen',
    },
    USER_LIST: {
      TITLE: 'Mitspieler',
    },
  },
  WELCOME: {
    DESCRIPTION: `<p><strong>Smudgy</strong> ist ein kostenfreies Multiplayer <strong>Montagsmaler</strong> Spiel.
Spiele es <strong>zusammen</strong> mit Deinen <strong>Freunden</strong>!
Eine Spielrunde besteht aus mehreren <strong>Mal</strong>- und <strong>Rate</strong>runden. In jeder Mal- und Raterunde wird ein Spieler ans Zeichenbrett
gebeten, um ein Wort zu <strong>malen</strong>, was von den anderen Mitspielern <strong>erraten</strong> werden muss.</p>

<p>Wer als erstes errät, bekommt die meisten Punkte. Am Ende <strong>gewinnt</strong>, wer die <strong>meisten</strong> Punkte hat!</p>`,
    TITLE: 'Willkommen!',
  },
  USER_INFORMATION: {
    PLAYER_NAME: 'Spielername',
    PLAY: 'Jetzt spielen!',
  },
  FOOTER: {
    A_GAME_BY: 'Smudgy ist ein Spiel von',
    DISCLAIMER: 'Der Eigentümer der Seite ist nicht verantwortlich für spielergenerierter Inhalt (Zeichnungen, Nachrichten, Spielernamen)',
  },
};
