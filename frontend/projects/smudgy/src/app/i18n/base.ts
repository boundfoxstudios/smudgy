export interface Translations {
  USER_INFORMATION: {
    PLAYER_NAME: string;
    PLAY: string;
  };
  WELCOME: {
    TITLE: string;
    DESCRIPTION: string;
  };
  FOOTER: {
    A_GAME_BY: string;
    DISCLAIMER: string;
  };
  LOBBY: {
    SETTINGS: {
      TITLE: string;
    };
    USER_LIST: {
      TITLE: string;
    };
  };
}
