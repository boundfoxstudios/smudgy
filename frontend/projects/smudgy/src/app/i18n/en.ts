import { Translations } from './base';

export const TRANSLATIONS_EN: Translations = {
  LOBBY: {
    SETTINGS: {
      TITLE: 'Settings',
    },
    USER_LIST: {
      TITLE: 'Fellow players',
    },
  },
  WELCOME: {
    DESCRIPTION: `MISSING TRANSLATION`,
    TITLE: 'Welcome!',
  },
  USER_INFORMATION: {
    PLAYER_NAME: 'Player name',
    PLAY: 'Start playing!',
  },
  FOOTER: {
    A_GAME_BY: 'Smudgy is a game by',
    DISCLAIMER: 'The owner of this site is not responsible for any user generated content (drawings, messages, usernames)',
  },
};
